import yaml
import json
import pandas as pd
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

def load_config(config_path):
    logger.info(f"Loading config from {config_path}")
    with open(config_path, 'r') as file:
        config = yaml.safe_load(file)
    return config

def load_data(data_settings):
    datasets = []
    for dataset in data_settings.get('seed_datasets', []):
        file_path = dataset.get('file_path')
        file_format = dataset.get('format')
        
        if file_format == 'json':
            with open(file_path, 'r') as file:
                data = json.load(file)
                logger.info(f"Loaded {len(data)} records from {file_path}")
                datasets.append(data)
        elif file_format == 'csv':
            data = pd.read_csv(file_path)
            logger.info(f"Loaded {len(data)} records from {file_path}")
            datasets.append(data)
        else:
            logger.error(f"Unsupported file format: {file_format}")
            raise ValueError(f"Unsupported file format: {file_format}")
    
    return datasets

if __name__ == "__main__":
    logger.info(' == Running ... ==')
    config_path = "configs/code_gen_evoli.yaml"
    config = load_config(config_path)
    datasets = load_data(config['data_settings'])
    """     for dataset in datasets:
        print(dataset) """