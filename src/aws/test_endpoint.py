import random
from TextGenerationInference import TGI, GenerateRequest, GenerateParameters
import json 
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from nltk.util import ngrams
from nltk.metrics import edit_distance
import logging
import yaml
from tqdm import tqdm as tqdm_bar


# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

# Load the configuration from the .yaml file
def load_config(config_path):
    with open(config_path, 'r') as file:
        config = yaml.safe_load(file)
    return config


tgi_client = TGI(endpoint_name="CodeLlama-70b-hf-endpoint2", region_name="us-east-1")

question = 'Create an array of length 5 which contains all even numbers between 1 and 10.'


def batch_generate(model, params, prompts, batch_size):
    all_results = []
    for i in tqdm_bar(range(0, len(prompts), batch_size), desc="Generating"):
        batch_prompts = prompts[i: i + batch_size]
        requests = [GenerateRequest(inputs=prompt, parameters=params) for prompt in batch_prompts]
        batch_responses = model.create_from_objects(requests)
        all_results.extend(batch_responses)
    return all_results

params = GenerateParameters(max_new_tokens=512, temperature=0.7)
request = GenerateRequest(inputs=question, parameters=params)
response = tgi_client.create_from_objects([request])[0]


print(response)