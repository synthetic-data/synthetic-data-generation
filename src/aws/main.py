import os
import yaml
from argparse import ArgumentParser

def load_config(config_path):
    with open(config_path, 'r') as file:
        config = yaml.safe_load(file)
    return config

if __name__ == '__main__':
    parser = ArgumentParser(description="Main Deploy Model Script")
    args = parser.parse_args()

    config_path = os.path.join(os.path.dirname(__file__), '..', '..', 'configs', 'code_gen_evoli.yaml')
    config = load_config(config_path)

    model_location = config['model_settings']['model_selection'][0]['location']

    if model_location == 'HuggingFace':
        os.system(f'python3 hf_deploy_endpoint.py')
    elif model_location == 'AWS':
        os.system(f'python3 aws_deploy_endpoint.py')
    else:
        raise ValueError(f"Unknown model location: {model_location}")
