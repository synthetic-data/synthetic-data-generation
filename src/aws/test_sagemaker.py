import boto3
import json

# Define the endpoint name
endpoint_name = 'your-endpoint-name'

# Create a SageMaker runtime client
runtime_client = boto3.client('sagemaker-runtime')

# Define your question
question = 'Give me the most famous rock songs of all time'

# Define the payload
payload = {
    'question': question
}

# Convert the payload to JSON
payload_json = json.dumps(payload)

# Invoke the endpoint
response = runtime_client.invoke_endpoint(
    EndpointName=endpoint_name,
    ContentType='application/json',
    Body=payload_json
)

# Parse the response
response_body = response['Body'].read().decode('utf-8')
response_json = json.loads(response_body)

# Include the question in the response JSON
response_json['question'] = question

# Print the response in JSON format
print(json.dumps(response_json, indent=4))