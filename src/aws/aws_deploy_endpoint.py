import os
import time
import sagemaker
import boto3
import json
import yaml
import argparse
from omegaconf import OmegaConf
from sagemaker.huggingface.model import HuggingFaceModel
import logging
from argparse import ArgumentParser

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

# Function to launch the job (replace this with your actual implementation)
def launch_job(config, instance_type, container_image_uri, volume_size, max_run, base_job_name):
    # Your implementation here
    pass


# Function to convert checkpoint
def hf_convert_checkpoint(cp_path, tokenizer="noor_v3_mix/", instance_type='ml.p4d.24xlarge', volume=300, max_run=24 * 60 * 60, container_image_uri='tii-eval:latest'):
    config = OmegaConf.create()
    config.converter = {}
    config.converter.path = cp_path
    config.converter.tokenizer = os.path.join('assets/tokenizer', tokenizer)
    config.type = "converter"

    launch_job(
        config,
        instance_type=instance_type,
        container_image_uri=container_image_uri,
        volume_size=volume,
        max_run=max_run,
        base_job_name="sagemaker-converter",
    )

# Function to deploy the model
def deploy_tgi_model(config, args):
    
    try:
        role = sagemaker.get_execution_role()
    except ValueError:
        iam = boto3.client('iam')
    role = iam.get_role(RoleName="SMRole")["Role"]["Arn"]
    boto3_session = boto3.session.Session(region_name=args.region)
    sess = sagemaker.Session(boto3_session)


    model_path = config['model_settings']['model_selection']['path']
    instance_type = config['model_settings']['aws_settings']['instance_type']
    endpoint_name = config['model_settings']['aws_settings']['endpoint_name']

    hub = {
        'HF_MODEL_ID': model_path,
        'SM_NUM_GPUS': json.dumps(1 if args.size == '7b' else 8),
        'SAGEMAKER_MODEL_SERVER_TIMEOUT': '900000',
        'MAX_INPUT_LENGTH': '2048',
        'MAX_TOTAL_TOKENS': '2049',
        'MAX_BATCH_PREFILL_TOKENS': '8192',
        'MAX_STOP_SEQUENCES': '10'
    }
    if args.size != '7b':
        hub.update({'SHARDED': 'true', 'NUM_SHARD': '8'})

    os.environ['AWS_DEFAULT_REGION'] = args.region
    account_id = boto3.client('sts').get_caller_identity()['Account']
    huggingface_model = HuggingFaceModel(
        image_uri=f"{account_id}.dkr.ecr.{args.region}.amazonaws.com/tgi-tii-sm:latest",
        model_data=os.path.join(args.path, "model.tar.gz"),
        env=hub,
        role=role,
        sagemaker_session=sess,
    )

    print(f"Waiting for {endpoint_name} endpoint to be in service...")
    huggingface_model.deploy(
        endpoint_name=endpoint_name,
        initial_instance_count=1,
        instance_type=instance_type,
        container_startup_health_check_timeout=300,
        trust_remote_code=True,
    )
    print("=" * 20)
    print("Endpoint name: " + endpoint_name)
    print("=" * 20)
    return endpoint_name

if __name__ == '__main__':
    parser = ArgumentParser(description="Deploy Model Arguments")
    parser.add_argument('--instance', default='ml.p4d.24xlarge', help='Instance type for eval')
    parser.add_argument('-t', '--tokenizer', default='dd_alpha', help='Tokenizer for conversion to HF weights')
    parser.add_argument('--timestamp', action=argparse.BooleanOptionalAction, default=True, help='Append timestamp to endpoint name')
    parser.add_argument('-r', '--region', default='us-west-2', help='AWS region')

    args = parser.parse_args()
    if 'us-east-1' in args.region:
        args.region = 'us-east-1'
    elif 'us-west-2' in args.region:
        args.region = 'us-west-2'

    config_path = os.path.join(os.path.dirname(__file__), '..', '..', 'configs', 'code_gen_evoli.yaml')
    with open(config_path, 'r') as file:
        config = yaml.safe_load(file)

    args.path = config['model_settings']['model_selection'][0]['path']
    args.name = config['model_settings']['aws_settings']['endpoint_name']
    args.size = config['model_settings']['aws_settings']['size']

    if not os.path.exists(os.path.join(args.path, "model.tar.gz")):
        hf_convert_checkpoint(cp_path=args.path, tokenizer=args.tokenizer.split("/")[0])

    deploy_tgi_model(config, args)
