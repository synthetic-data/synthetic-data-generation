import json
import sagemaker
import boto3
from sagemaker.huggingface import HuggingFaceModel, get_huggingface_llm_image_uri
import yaml
import logging
import os

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

# Load the configuration from the .yaml file
def load_config(config_path):
    with open(config_path, 'r') as file:
        config = yaml.safe_load(file)
    return config

# Load the config file
config_path = os.path.join(os.path.dirname(__file__), '..', '..', 'configs', 'code_gen_evoli.yaml')
config = load_config(config_path)

# Define the settings
model_path = config['model_settings']['model_selection']['path']
instance_type = config['model_settings']['aws_settings']['instance_type']
endpoint_name = config['model_settings']['aws_settings']['endpoint_name']
region = config['model_settings']['aws_settings']['region']

# ------ Deploy endpoint ---- #
try:
    role = sagemaker.get_execution_role()
except ValueError:
    iam = boto3.client('iam')
    role = iam.get_role(RoleName="SMRole")["Role"]["Arn"]

# Hub Model configuration. https://huggingface.co/models
hub = {
    'HF_MODEL_ID': model_path,
    'SM_NUM_GPUS': '4',
    'MAX_STOP_SEQUENCES': '10'
}

# create Hugging Face Model Class
huggingface_model = HuggingFaceModel(
    image_uri=get_huggingface_llm_image_uri("huggingface", version="1.4.2"),
    env=hub,
    role=role,
)

# deploy model to SageMaker Inference
try:
    predictor = huggingface_model.deploy(
        initial_instance_count=1,
        instance_type=instance_type,
        container_startup_health_check_timeout=300,
        endpoint_name=endpoint_name
    )
    logger.info(f"Endpoint {endpoint_name} deployed successfully.")
except Exception as e:
    logger.error(f"Failed to deploy endpoint: {e}")
    raise

# send request
try:
    response = predictor.predict({
        "inputs": "My name is Clara and I am",
    })
    logger.info(f"Prediction response: {response}")
except Exception as e:
    logger.error(f"Failed to send prediction request: {e}")
    raise
# ------ Deploy endpoint ----