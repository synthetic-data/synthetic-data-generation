1. Create code to deploy an endpoint from AWS S3 path and HF
2. Create code to start a sagemaker job, calling this endpoint

## How to use

### Deploy an endpoint from a HuggingFace Model

1. Go to ```configs/code_gen_evoli.yaml```
2. Change the following according to your settings :
```yaml
model_settings: 
  model_selection:
    - location: "HuggingFace" #or AWS
      path: "codellama/CodeLlama-70b-hf" #or s3://example-bucket/path/to/model"
  aws_settings:
    instance_type: "ml.m5.xlarge"
    endpoint_name: "CodeLlama-70b-hf-endpoint"
```
3. Run ```main_deploy_endpoints.py```
Depending on if you specified 'AWS' or 'HuggingFace', this script will automatically start an endpoint by calling either ```hf_deploy_endpoint.py``` or ```aws_deploy_endpoint.py```