import os
import random
import time
import yaml
import json
import pandas as pd
import logging
from src.loaders.data_loader import load_data

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

# SKELETON
# variables in functions need to be generic enough to be specified later in generate.py when this script is being called
# Load seed dataset -> def load_instructions() ✅
# If the format is [instruction, input, output], put input in instruction to get [instruction, output] -> def convert_alpaca_to_evol()
# Evolve instructions by loading the prompts evoli.yaml -> def evolve_instructions()
# Generate response -> def generate_response()
# -> def generate_set()
# -> if main then call generate_set()

# Load the config file
def load_config(config_path):
    with open(config_path, 'r') as file:
        config = yaml.safe_load(file)
    return config


def generate_output_file_name(file_path, suffix="_converted"):
    base_name = os.path.basename(file_path)
    name, ext = os.path.splitext(base_name)
    new_name = name + suffix
    output_file = os.path.join(os.path.dirname(file_path), new_name + ext)
    
    return output_file


def convert_alpaca_to_evol(
    file_path: str, 
    lines: bool = False,
    output_file: str = None
):
    if output_file is None:
        output_file = generate_output_file_name(file_path)
    """Convert the Instruction/Input/Output format of Alpaca Instruct datasets
    to the Evol-Instruct format of Instruction/Output. Inputs are appended to the
    instructions.
    
    Args:
        file_path: the file path to a single JSON file in alpaca format
        lines: Set to True if the input is a JSONL file, the default is False
        
    Returns: a list of the instruction-output pairs generated from the alpaca set
    Ref : https://github.com/nickrosh/evol-teacher/blob/main/generate_evol.py"""
    
    result = []
    if lines:
        with open(file_path, "r") as json_file:
            loaded_json = [json.loads(line) for line in json_file]
        for record in loaded_json:
            if record["instances"][0]["input"]:
                record["instruction"] += '\n' + record["instances"][0]["input"]
            result.append({
                "instruction": record["instruction"],
                "output": record["instances"][0]["output"]
            })
    else:
        with open(file_path, "r") as json_file:
            loaded_json = json.load(json_file)
        for record in loaded_json:
            if record["input"]:
                record["instruction"] += '\n' + record["input"]
            result.append({
                "instruction": record["instruction"],
                "output": record["output"]
            })
    with open(output_file, "w") as fp:
        json.dump(result, fp)
    logger.info(f" {output_file} has been created")
    return result

def evolve_instruction(original_instructions, methods, prompt_template):
    instructions_evolution = []
    prompts = []

    for instruction_list in original_instructions:  # Iterate over each list in the list
        for instruction in instruction_list:  # Iterate over each dictionary in the list
            #if isinstance(instruction, dict) and 'instruction' in instruction:
                inst = instruction['instruction']
                prompt = prompt_template.format(method=random.choice(methods), instruction=inst)
                prompts.append(prompt)
            #else:
            #    print(f"Unexpected type or structure within instruction_list: {instruction}")

    return prompts


    
#def generate_responses(new_instructions, model):    

def generate_set():
    prompts_config = load_config('configs/prompts_evoli.yaml')
    general_config = load_config('configs/code_gen_evoli.yaml')

    technique_settings = prompts_config['technique_settings']
    methods = technique_settings['methods']
    prompt_template = technique_settings['prompt_template']

    #logger.info(f"The methods are : {methods}")
    #logger.info(f"The main prompt is : {prompt_template}")


    dataset = load_data(general_config['data_settings'])
    #logger.info(f"DATASET : {dataset[0][0]}")

    #prompt = prompt_template.format(method=random.choice(methods), instruction=dataset[0][0]['instruction'])
    #print(prompt)

    print(evolve_instruction(dataset, methods, prompt_template)[1])

if __name__ == "__main__":
    #convert_alpaca_to_evol(file_path="data/seed_datasets/code_alpaca_2k.json")
    generate_set()






"""     import yaml

# Load the configuration file
with open('config.yaml', 'r') as file:
    config = yaml.safe_load(file)

# Extract the necessary parts of the configuration
technique_settings = config['technique_settings']
methods = technique_settings['methods']
prompt_template = technique_settings['prompt_template']

# Example dynamic values for method and question
method = methods[0]  # You can choose or randomly select a method from the list
question = "Write a function to reverse a string."

# Format the prompt template with the dynamic values
prompt = prompt_template.format(method=method, question=question)

print(prompt) """



"""     # == CONFIGURATIONS == #
    # Load the configuration file for the prompts
    prompts_config = load_config('src/generation_techniques/code/prompts_evoli.yaml')
    # Load the configuration file for loading a specific dataset
    general_config = load_config('configs/code_gen_evoli.yaml')

    # Extract the necessary parts of the configuration
    technique_settings = prompts_config['technique_settings']
    technique = technique_settings['technique']
    methods = technique_settings['methods']
    prompt_template = technique_settings['prompt_template']
    #logger.info(f"Loading the methods and prompts from the {technique} technique")
    #logger.info(f"The methods are : {methods}")
    #logger.info(f"The main prompt is : {prompt_template}")

    # Load data from the specified file
    dataset = load_data(general_config['data_settings'])
    #print("Datasets loaded:", dataset)  # Debug print

    # == VARIABLES == #
    n = 3 # nb of evolutions

    # == START GENERATION == #
    # Instructions 
    print(evolve_instruction(dataset, methods, prompt_template))
    # Code """