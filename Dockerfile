# Use an official Python runtime as a parent image
FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
# Assuming you have a requirements.txt file listing all dependencies
# COPY requirements.txt /app
# RUN pip install --no-cache-dir -r requirements.txt

# Install the necessary Python packages
RUN pip install --no-cache-dir pandas pyyaml

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World

# Run main.py when the container launches
CMD ["python3", "src/generation_techniques/code/evol_instruct.py"]