# 🧪👩‍🔬 Synthetic Data Generation Software 

## How to use

1. Create a virtual environment:

```sh
conda create -n synthetic-gen python=3.11
```

2. Activate the venv:

```sh
conda activate synthetic-gen
```

3. Updata pip and Install the requirements: 

```sh
python3.11 -m pip install --upgrade pip
pip3 install -r requirements.txt
```

### Run Data Loader

1. Be sure to be at the root of the repo and run: 

```sh
python3 src/loaders/data_loader.py
```


### Docker 

1. Build the image: 

```sh
docker build -t your_image_name .
```

2. Run the image: 

```sh
docker run -p 4000:80 your_image_name
```

3. Optional : 

```sh
docker-compose up
```


## Directory Tree

&rarr; data/ : Sample data files for testing. \
&rarr; src/ Source code of the project : 
- data_loader